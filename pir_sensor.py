import RPi.GPIO as GPIO


# Настройка пина, на который подключен датчик движения:
GPIO_PIR = 4
GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIR, GPIO.IN)


# Настройка интервала опроса датчика:
PIR_STATE_READING_INTERVAL = 1

pir_previous_state = 0


def read_sensor_state():
    global pir_previous_state
    pir_sensor_state = GPIO.input(GPIO_PIR)
    if pir_sensor_state == 1 and pir_previous_state == 0:
        pir_previous_state = 1
        return True
    pir_previous_state = 0
    return False
