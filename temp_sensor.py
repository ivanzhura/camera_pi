import board
import adafruit_ahtx0


# Create sensor object, communicating over the board's default I2C bus
i2c = board.I2C()  # uses board.SCL and board.SDA
sensor = adafruit_ahtx0.AHTx0(i2c)


def read_temp_and_humidity():
    # поправка в 8 градусов нужна, чтобы компенсировать нагрев корпуса от
    # камеры и Raspberry (установлена экспериментально)
    return round(sensor.temperature - 8), round(sensor.relative_humidity)
