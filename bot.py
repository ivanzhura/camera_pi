from datetime import datetime
import logging
import asyncio

import tzlocal
from aiogram import Bot, Dispatcher, types
from aiogram.utils import exceptions
from apscheduler.schedulers.asyncio import AsyncIOScheduler

import config
import camera
import pir_sensor
import temp_sensor


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

bot = Bot(token=config.BOT_TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot)

scheduler = AsyncIOScheduler(timezone=str(tzlocal.get_localzone()))


def get_allowed_chat_ids():
    """Return users list"""
    yield from config.ALLOWED_CHAT_IDS


def check_authorization(func):
    """Decorator to check user authorization"""
    async def wrapper(message: types.Message):
        if message:
            if message.chat.id in get_allowed_chat_ids():
                await func(message)
            else:
                await bot.send_message(
                    config.ADMIN_CHAT_ID,
                    f"Попытка связи с ботом!\n"
                    f"Юзернейм: {message.chat.username}.\n"
                    f"Имя:{message.chat.first_name} {message.chat.last_name}.\n"
                    f"id чата: <b>{message.chat.id}</b>"
                )
        else:
            log.info("Fixed an attempt to contact the bot without a message")
    return wrapper


async def send_photo() -> bool:
    """Send on schedule photo for all bot users"""
    date, current_time = datetime.now().strftime("%d-%m-%Y %H:%M:%S").split(" ")
    photo_path = f"{config.PHOTO_PATH}/image{datetime.now()}.jpg"
    for chat_id in get_allowed_chat_ids():
        try:
            temperature, humidity = temp_sensor.read_temp_and_humidity()
            await bot.send_message(
                chat_id,
                f"Температура <b>{temperature} C</b>.\n"
                f"Влажность <b>{humidity} %</b>."
            )
            await bot.send_message(
                chat_id,
                f"<b>{date}</b> в <b>{current_time}</b>! Высылаю фото..."
            )
            camera.CAMERA.capture(photo_path)
            with open(photo_path, "rb") as photo:
                await bot.send_photo(chat_id=chat_id, photo=photo)
        except exceptions.BotBlocked:
            log.error(f"Target [ID:{chat_id}]: blocked by user")
        except exceptions.ChatNotFound:
            log.error(f"Target [ID:{chat_id}]: invalid user ID")
        except exceptions.RetryAfter as e:
            log.error(
                f"Target [ID:{chat_id}]: Flood limit is exceeded. Sleep {e.timeout} seconds.")
            await asyncio.sleep(e.timeout)
            return await send_photo()  # Recursive call
        except exceptions.UserDeactivated:
            log.error(f"Target [ID:{chat_id}]: user is deactivated")
        except exceptions.TelegramAPIError:
            log.exception(f"Target [ID:{chat_id}]: failed")
        else:
            log.info(f"Target [ID:{chat_id}]: success")


@dp.message_handler(commands=["start"])
@check_authorization
async def send_welcome(message: types.Message):
    """This handler will be called when user sends `/start` command"""
    await message.reply(
        "Привет. Я бот видеонаблюдения!\n"
        "По умолчанию я отправляю фото каждые 30 мин, "
        "а видео в случае обнаружения движения.\n"
        "Ты можешь попросить меня отправить фото или видео "
        "принудительно по командам: \n"
        "   /send_photo - для фото\n"
        "   /send_video - для видео."
        "Напиши /help чтобы посмотреть, что еще я умею."
    )


@dp.message_handler(commands=["help"])
@check_authorization
async def send_help(message: types.Message):
    """This handler will be called when user sends `/help` command"""
    await message.reply(
        "Напиши /help чтобы посмотреть, что я умею.\n"
        "Напиши /send_video чтобы отправить 10-ти секундное видео.\n"
        "Напиши /send_photo чтобы принудительно отправить фото.\n"
        "Напиши /send_temperature чтобы получить актуальную температуру и влажность.\n"
        "Напиши /change_photo_interval чтобы задать интервал между фото.\n"
        "Напиши /change_video_duration чтобы изменить длительность видео.\n"
    )


@dp.message_handler(commands=["change_photo_interval"])
@check_authorization
async def set_photo_interval(message: types.Message):
    """
    Command for changing the interval between sending photos.
    To use the command, you need to pass the number of seconds as an argument:
    For example: `/change_photo_interval 600` - set the 600 seconds interval
    """
    args = message.get_args()
    if args:
        try:
            config.PHOTO_INTERVAL = int(args)
            scheduler.remove_all_jobs()
            scheduler.add_job(send_photo, "interval", seconds=config.PHOTO_INTERVAL)
            await message.reply(
                "Интервал между фотографиями изменен!\n"
                f"Новое значение: <b>{config.PHOTO_INTERVAL} сек.</b>"
            )
        except ValueError:
            await message.reply(
                "Пожалуйста передайте число\n"
                "Пример: /change_photo_interval 60"
            )
    else:
        await message.reply(
            "Необходимо передать число\n"
            "Пример (установит интервал в 10 мин): /change_photo_interval 600"
        )


@dp.message_handler(commands=["change_video_duration"])
@check_authorization
async def set_video_default_duration(message: types.Message):
    """
    Command for changing the default video_duration.
    To use the command, you need to pass the number of seconds as an argument:
    For example: `/change_video_duration 60` - set the 60 seconds video duration
    """
    # TODO: Проверить максимальную длительность видео. Есть ограничение на размер.
    args = message.get_args()
    if args:
        try:
            config.VIDEO_DURATION = int(args)
            await message.reply(
                "Длительность видео по умолчанию изменена\n"
                f"Новое значение: <b>{config.VIDEO_DURATION} сек.</b>"
            )
        except ValueError:
            await message.reply(
                "Пожалуйста передайте число\n"
                "Пример: /change_video_duration 60"
            )
    else:
        await message.reply(
            "Необходимо передать число\n"
            "Пример (установит интервал в 60 сек): /change_video_duration 60"
        )


@dp.message_handler(commands=["send_video"])
@check_authorization
async def send_video_force(message: types.Message):
    """
    Handler for `/send_video` command. Bot will send the 10 second of video
    as reply
    """
    args = message.get_args()
    if args:
        try:
            video_duration = int(args)
            await message.reply("Записываю видео...")
            h264_video_path = camera.record_video(
                camera.CAMERA,
                video_duration=video_duration
            )
        except ValueError:
            return await message.reply(
                "Длительность видео должна быть задана целым числом (секунды)!"
            )
    else:
        await message.reply("Записываю видео...")
        h264_video_path = await camera.record_video(camera.CAMERA)
    mp4_video_path = await camera.convert_video_to_mp4(h264_video_path)
    with open(mp4_video_path, "rb") as video:
        # await message.reply_video(video, width=1920, height=1080)  # разницы нет
        await message.reply_video_note(video)


@dp.message_handler(commands=["send_photo"])
@check_authorization
async def send_photo_force(message: types.Message):
    """Handler for `/send_photo` command. Bot will send the photo as reply"""
    photo_path = f"{config.PHOTO_PATH}/image{datetime.now()}.jpg"
    await camera.CAMERA.capture(photo_path)
    with open(photo_path, "rb") as photo:
        await message.reply_photo(photo=photo)


@dp.message_handler(commands=["send_temperature"])
@check_authorization
async def send_temp_humidity(message: types.Message):
    """
    Handler for `/send_temperature` command. Bot will send current
    temperature and humidity as reply
    """
    temperature, humidity = temp_sensor.read_temp_and_humidity()
    await message.reply(
        f"Температура <b>{temperature} C</b>.\n"
        f"Влажность <b>{humidity} %</b>."
    )


async def send_pir_sensor_state():
    if pir_sensor.read_sensor_state():
        date, current_time = datetime.now().strftime("%d-%m-%Y %H:%M:%S").split(" ")
        h264_video_path = camera.record_video(camera.CAMERA)
        for chat_id in get_allowed_chat_ids():
            await bot.send_message(
                chat_id,
                f"<b>{date}</b> в <b>{current_time}</b> зафиксировано движение!"
            )

        for chat_id in get_allowed_chat_ids():
            mp4_video_path = camera.convert_video_to_mp4(h264_video_path)
            with open(mp4_video_path, "rb") as video:
                await bot.send_video(chat_id=chat_id, video=video, width=1920, height=1080)


async def main():
    for chat_id in get_allowed_chat_ids():
        await bot.send_message(
            chat_id,
            f"Бот был перезапущен"
        )
    await send_photo()
    scheduler.add_job(send_photo, "interval", seconds=config.PHOTO_INTERVAL)
    scheduler.add_job(
        send_pir_sensor_state,
        "interval",
        seconds=pir_sensor.PIR_STATE_READING_INTERVAL
    )
    scheduler.start()
    await dp.start_polling(bot)

if __name__ == "__main__":
    asyncio.run(main())
