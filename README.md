# Raspberry Pi Camera Telegram bot

This Telegram bot was created using `Python`, 
as well as the `aiogram` and `apscheduler` libraries.

At the moment, the functionality is implemented in the software:
* Sending photos on a schedule.
* Force sending a message to a chat.

## Example `config.py`:
<code>
BOT_TOKEN = "YOURS TOKEN"
ALLOWED_CHAT_IDs = (
    634243546,
    145457843,
    ...
)
PHOTO_PATH = "/home/pi/camera_pi/photos"
VIDEO_PATH = "/home/pi/camera_pi/mp4_videos"
H264_VIDEOS_PATH = "/home/pi/camera_pi/raw_videos"
LOG_PATH = "log.txt"
</code>

## Example `crontab` schedule:
<code>
0 * * * * bash /home/pi/camera_pi/scripts/check_updates.sh
0 0 1 * * bash /home/pi/camera_pi/scripts/cleanup_photos.sh
0 1 1 * * bash /home/pi/camera_pi/scripts/cleanup_videos.sh
</code>

## Example `systemd` config:

`/etc/systemd/system/camera_bot.service`:

<code>
[Unit]
Description=Camera bot service
After=multi-user.target
	 
[Service]
User=pi
Type=simple
Restart=always
ExecStart=python3 /home/pi/camera_pi/bot.py 
	 
[Install]
WantedBy=multi-user.target

</code>