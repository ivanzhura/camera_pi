import os
import logging
from datetime import datetime
from subprocess import call as sp_call
from picamera import PiCamera
from config import H264_VIDEOS_PATH, MP4_VIDEO_PATH, VIDEO_DURATION


# объект для работы с камерой
CAMERA = PiCamera()
CAMERA.resolution = (1920, 1080)
CAMERA.rotation = 180

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def record_video(camera_object: PiCamera, video_duration: int = VIDEO_DURATION):
    h264_video_path = os.path.join(H264_VIDEOS_PATH, f'{datetime.now().timestamp()}.h264')
    log.info(f'Записываю видео: {h264_video_path}')
    camera_object.start_recording(h264_video_path, bitrate=8000000, quality=30)
    camera_object.wait_recording(video_duration)
    camera_object.stop_recording()
    return h264_video_path


def convert_video_to_mp4(h264_video_path):
    mp4_video_path = os.path.join(MP4_VIDEO_PATH, f'{datetime.now().timestamp()}.mp4')
    convert_command = ['MP4Box', '-add', h264_video_path, mp4_video_path]
    log.info('Конвертация в MP4...')
    sp_call(convert_command)
    log.info(f'Видео сохранено в файл: {mp4_video_path}')
    return mp4_video_path
